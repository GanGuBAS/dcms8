<?php

namespace Dcms\Misc;

use Dcms\Core\Modules;

/**
 * Различные полезные функции
 */
abstract class Misc
{

    /**
     *
     * @param int $num
     * @param string $one
     * @param string $two
     * @param string $more
     * @return string
     */
    public static function number($num, $one, $two, $more)
    {
        $num = (int) $num;
        $l2 = substr($num, strlen($num) - 2, 2);

        if ($l2 >= 5 && $l2 <= 20) return $more;
        $l = substr($num, strlen($num) - 1, 1);
        switch ($l) {
            case 1:
                return $one;
            case 2:
                return $two;
            case 3:
                return $two;
            case 4:
                return $two;
            default:
                return $more;
        }
    }

    /**
     * Вычисление возраста
     * @param int $g Год
     * @param int $m Месяц
     * @param int $d День
     * @param boolean $read
     * @return string
     */
    public static function getAge($g, $m, $d, $read = false)
    {
        if (strlen($g) == 2) $g += 1900;
        if (strlen($g) == 3) $g += 1000;
        $age = date('Y') - $g;
        if (date('n') < $m) $age--; // год не полный, если текущий месяц меньше
        elseif (date('n') == $m && date('j') < $d)
                $age--; // год не полный, если текущий месяц совпадает, но день меньше
        if ($read) return $age.' '.self::number($age, __('год'), __('года'), __('лет'));

        return $age;
    }

    /**
     * Читабельное представление размера информации
     * @param int $filesize размер в байтах
     * @return string размер в (KB, MB...)
     */
    public static function getDataCapacity($filesize = 0)
    {
        $filesize_ed = __('байт');
        if ($filesize >= 1024) {
            $filesize = round($filesize / 1024, 2);
            $filesize_ed = __('Кб');
        }
        if ($filesize >= 1024) {
            $filesize = round($filesize / 1024, 2);
            $filesize_ed = __('Мб');
        }
        if ($filesize >= 1024) {
            $filesize = round($filesize / 1024, 2);
            $filesize_ed = __('Гб');
        }
        if ($filesize >= 1024) {
            $filesize = round($filesize / 1024, 2);
            $filesize_ed = __('Тб');
        }
        if ($filesize >= 1024) {
            $filesize = round($filesize / 1024, 2);
            $filesize_ed = __('Пб');
        }

        return $filesize.' '.$filesize_ed;
    }

    /**
     * читабельное представление времени с учетом часового пояса пользователя
     * 
     * @param int $time Время в формате timestamp
     * @return string
     */
    public static function date($format, $timestamp = null)
    {
        $date = is_null($timestamp) ? date($format) : date($format, $timestamp);
        Modules::executeHook('misc.date',
            $hook_args = array(
            'date' => &$date,
            'format' => $format,
            'timestamp' => $timestamp
        ));
        return $date;
    }

    /**
     * @param int $len
     * @return string
     */
    public static function getRandomPhrase($len = 32)
    {
        $password = '';
        $small = 'abcdefghijklmnopqrstuvwxyz';
        $large = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $numbers = '1234567890';
        for ($i = 0; $i < $len; $i++) {
            switch (mt_rand(1, 3)) {
                case 3 :
                    $password .= $large [mt_rand(0, 25)];
                    break;
                case 2 :
                    $password .= $small [mt_rand(0, 25)];
                    break;
                case 1 :
                    $password .= $numbers [mt_rand(0, 9)];
                    break;
            }
        }
        return $password;
    }

    /**
     * @param array $array
     * @return bool
     */
    public static function isAssocArray($array)
    {
        return (bool) count(array_filter(array_keys($array), 'is_string'));
    }

    public static function checkLogin($login)
    {
        if (!is_string($login)) throw new \Exception(__('Не корректный тип значения'));

        if (mb_strlen($login) < 3) throw new \Exception(__('Длина %s не может быть менее %s символов', __('логина'), 3));

        if (preg_match('/^[0-9]/', $login)) throw new \Exception(__('Логин не должен начинаться с цифры'));

        // можно задавать свои правила для логина. Ошибку выдавать в Exception
        Modules::executeHook('misc.checkLogin', $login);
    }

    public static function checkPassword($password)
    {
        if (!is_string($password)) throw new Exception(__('Не корректный тип значения'));

        if (mb_strlen($password) < 6)
                throw new Exception(__('Длина %s не может быть менее %s символов', __('пароля'), 6));

        // можно задавать свои правила для пароля. Ошибку выдавать в Exception
        Modules::executeHook('misc.checkPassword', $password);
    }
}