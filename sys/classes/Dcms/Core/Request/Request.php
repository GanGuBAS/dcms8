<?php

namespace Dcms\Core\Request;

use \Dcms\Core\Url;

abstract class Request
{
    const
        CTYPE_DEFAULT = 0,
        CTYPE_XML = 1,
        CTYPE_JSON = 2;

    static protected
        $_path;

    /**
     * Метод запроса
     * @return string
     */
    static public function getMethod()
    {
        return strtolower($_SERVER['REQUEST_METHOD']);
    }

    /**
     * Путь страницы без GET параметров
     * @return string
     */
    static public function getPath()
    {
        if (is_null(self::$_path)) {
            self::$_path = '/'.filter_input(INPUT_GET, ROUTE_PROP_NAME);
        }
        return self::$_path;
    }

    /**
     * Текущий URL
     * @return Url
     */
    static public function getUrl()
    {
        $url = new Url(self::getPath(), filter_input_array(INPUT_GET));
        $url->removeParam(ROUTE_PROP_NAME);
        return $url;
    }

    /**
     * Модификация параметра втекущем запросе.
     * Вернется путь с модифицированным параметром
     * @param string $name
     * @param string $value
     * @return Url
     */
    static public function setParam($name, $value)
    {
        return self::getUrl()->setParam($name, $value);
    }

    /**
     * Возвращает GET параметр текущего запроса
     * @param string $name
     * @param mixed $default
     * @return mixed
     */
    static public function getParam($name, $default = null)
    {
        return self::getUrl()->getParam($name, $default);
    }

    /**
     * Значение переменной POST запроса
     * @param $name
     * @return mixed
     */
    static public function getPostVar($name)
    {
        return filter_input(INPUT_POST, $name);
    }

    /**
     * Ключ клиентской модели
     * @return string|null
     */
    static public function getModelKey()
    {
        return array_key_exists('HTTP_X_DCMS_MODEL_KEY', $_SERVER) ? $_SERVER['HTTP_X_DCMS_MODEL_KEY'] : null;
    }

    /**
     * Тип содержимого, который ожидает клиент.
     * TODO: сделать учет параметра q в заголовке Accept
     * @return int
     */
    static public function getAccept()
    {
        if (array_key_exists('HTTP_ACCEPT', $_SERVER)) {
            $accepts = explode(',', $_SERVER['HTTP_ACCEPT']);
            foreach ($accepts as $accept) {
                $accept = explode(';', trim($accept));
                switch ($accept[0]) {
                    case 'text/html':
                    case 'application/xhtml+xml':
                        return self::CTYPE_DEFAULT;
                    case 'application/xml':
                    case 'text/xml':
                        return self::CTYPE_XML;
                    case 'application/json':
                    case 'text/json':
                        return self::CTYPE_JSON;
                }
            }
        }
        return self::CTYPE_DEFAULT;
    }
}