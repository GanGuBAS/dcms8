<?php

namespace Dcms\Core;

/**
 * Реализация отрисовки модели указанной вьюхой
 * Class HtmlRender
 */
class UiModelRender
{
    public $id;

    function __construct()
    {
        $this->id = $this->_getNewId();
    }

    /**
     * Возвращает уникальный идентификатор класса на странице
     * @staticvar array $id
     * @return string
     */
    protected function _getNewId()
    {
        static $id = array();
        $class = str_replace('\\', '_', get_class($this));
        return $class.'_'.@ ++$id[$class];
    }

    /**
     * Отрисовка указанной вьюхи по текущей модели
     * @param string $view
     * @return string HTML
     */
    public function render($view)
    {
        $render = ThemeRender::getInstance();
        return $render->render($this, $view);
    }
}