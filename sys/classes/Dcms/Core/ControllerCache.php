<?php

namespace Dcms\Core;

use Dcms\Models\Data\Module;

/**
 * Хранение кэша контроллера
 * Изменения пишутся в файл перед завершением работы скрипта.
 * Сохранение параметров производится быстрее, чем в controller_settings
 * Рекомендуется использовать только для временных данных, которые не жалко потерять
 * Class controller_cache
 */
class ControllerCache
{
    protected $_cache_name;

    /**
     * @param $module Module
     */
    function __construct(Module $module)
    {
        $this->_cache_name = 'module-'.$module->getName();
    }

    /**
     * Получение значения из кэша
     * @param $name Имя параметра
     * @param mixed $default Это значение вернется, если отсутствует в кэше
     * @return mixed
     */
    function get($name, $default = false)
    {
        return Cache::get($this->_cache_name.'-'.$name, $default);
    }

    /**
     * Запись значения в кэш
     * @param string $name Имя параметра
     * @param mixed $value Значение
     * @param int $ttl Время жизни (в секундах)
     * @return bool
     */
    function set($name, $value, $ttl = 0)
    {
        return Cache::set($this->_cache_name.'-'.$name, $value, $ttl);
    }

    /**
     *
     * @param string $name
     * @return bool
     */
    function clear($name)
    {
        return Cache::clear($this->_cache_name.'-'.$name);
    }
}