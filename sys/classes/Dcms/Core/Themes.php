<?php

namespace Dcms\Core;

use Dcms\Models\Data\Theme;

/**
 * Список тем оформления
 */
abstract class Themes
{

    /**
     * Возвращает список установленных тем оформления
     * return Theme[]
     */
    public static function getInstalled()
    {
        $themes   = array();
        $settings = Settings::getSettings('themes');
        foreach ($settings as $theme_name => $theme_config) {
            try {
                $themes[] = new Theme(THEMES_PATH.'/'.$theme_name);
            } catch (\Exception $e) {
                
            }
        }
        return $themes;
    }

    /**
     * Возвращает одну из поддерживаемых тем оформления среди установленных
     * @return Theme
     */
    public static function getSupportedTheme()
    {
        $themes = self::getInstalled();
        return $themes[0];
    }
}