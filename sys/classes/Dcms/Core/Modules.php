<?php

namespace Dcms\Core;

use Dcms\Core\Request\Request;
use Dcms\Core\Response\Response;
use Dcms\Models\Data\Module;
use Dcms\Models\Data\DatabaseStructure\Table;
use Dcms\Models\Data\HookResult;

/**
 * Управление модулями
 * Class modules
 */
abstract class Modules
{
    const SORT_BY_CONTROLLER = 0;
    const SORT_BY_REQUEST = 1;
    const SORT_BY_RESPONSE = 2;
    const SORT_BY_HOOK = 3;

    /* region Методы для сортировки модулей */

    static protected function _cmp_by_field($v1, $v2, $field)
    {
        return ($v1[$field] == $v2[$field]) ? 0 : ($v1[$field] > $v2[$field] ? 1 : -1);
    }

    static protected function _cmp_controller($v1, $v2)
    {
        return self::_cmp_by_field($v1, $v2, 'controller');
    }

    static protected function _cmp_request($v1, $v2)
    {
        return self::_cmp_by_field($v1, $v2, 'request');
    }

    static protected function _cmp_response($v1, $v2)
    {
        return self::_cmp_by_field($v1, $v2, 'response');
    }

    static protected function _cmp_hook($v1, $v2)
    {
        return self::_cmp_by_field($v1, $v2, 'hook');
    }
    /* endregion */

    /**
     * @return Module[]
     * @throws \Exception
     */
    static function getAll()
    {
        $modules = array();
        $od = opendir(MODULES_PATH);
        while ($dir_name = readdir($od)) {
            if ($dir_name{0} === '.') {
                continue;
            }
            try {
                $module = new Module(MODULES_PATH . '/' . $dir_name);
                //print_r($module->getRoutes());
                $modules[] = $module;
            } catch (\Exception $e) {
                throw $e;
            }
        }
        closedir($od);
        return $modules;
    }

    /**
     * @param int $sort
     * @return Module[]
     * @throws \Exception
     */
    static function getInstalled($sort = self::SORT_BY_CONTROLLER)
    {
        $modules = array();
        $settings = Settings::getSettings('modules');

        switch ($sort) {
            case self::SORT_BY_CONTROLLER:
                uasort($settings, array('self', '_cmp_controller'));
                break;
            case self::SORT_BY_REQUEST:
                uasort($settings, array('self', '_cmp_request'));
                break;
            case self::SORT_BY_RESPONSE:
                uasort($settings, array('self', '_cmp_response'));
                break;
            case self::SORT_BY_HOOK:
                uasort($settings, array('self', '_cmp_hook'));
                break;
        }

        foreach ($settings AS $module_name => $installed_config) {
            try {
                $modules[] = new Module(MODULES_PATH . '/' . $module_name, $installed_config);
            } catch (\Exception $e) {
                throw $e;
            }
        }

        return $modules;
    }


    /**
     * @return Module
     * @throws \Exception
     */
    static function getModuleByRequest()
    {
        $modules_by_request = self::getModulesByRequest();
        if (!count($modules_by_request)) {
            throw new \Exception(__("Отсутствует обработчик для запроса %s", Request::getPath()));
        }
        return $modules_by_request[0];
    }

    /**
     * @return Module[]
     * @throws \Exception
     */
    static function getModulesByRequest()
    {
        $modules_by_request = array();
        $modules = self::getInstalled();
        foreach ($modules AS $module) {
            if ($module->canExecuteRequest()) {
                $modules_by_request[] = $module;
            }
        }
        return $modules_by_request;
    }

    /**
     * @return Module[]
     * @throws \Exception
     */
    static function getRequestPreprocessModules()
    {
        $modules = self::getInstalled(self::SORT_BY_REQUEST);
        $rpm = array();
        foreach ($modules AS $module) {
            if ($module->getRequestPreprocessMethodName()) {
                $rpm[] = $module;
            }
        }
        return $rpm;
    }

    /**
     * @return Module[]
     */
    static function getResponsePostprocessModules()
    {
        $modules = self::getInstalled(self::SORT_BY_RESPONSE);
        $rpm = array();
        foreach ($modules AS $module) {
            if ($module->getResponsePostprocessMethodName()) {
                $rpm[] = $module;
            }
        }
        return $rpm;
    }

    /**
     * @param $hook_name string
     * @return Module[]
     */
    static function getModulesByHookName($hook_name)
    {
        $modules = self::getInstalled(self::SORT_BY_HOOK);
        $modules_with_hook = array();
        foreach ($modules AS $module) {
            if (in_array($hook_name, $module->getHooks())) {
                $modules_with_hook[] = $module;
            }
        }
        return $modules_with_hook;
    }

    /**
     * Предварительная обработка запроса
     */
    static function executePreprocess()
    {
        $request_preprocess_modules = self::getRequestPreprocessModules();
        foreach ($request_preprocess_modules AS $module) {
            $module->executePreprocess();
        }
    }

    /**
     * непосредственная обработка запроса. Выполнение get, post методов модуля
     * @return Response
     */
    static function executeRequest()
    {
        $response = new Response();
        $module = self::getModuleByRequest();
        $module->executeRequest($response);
        return $response;
    }

    /**
     * Постобработка ответа
     * @param Response $response
     */
    static function executePostprocess(Response $response)
    {
        $response_postprocess_modules = self::getResponsePostprocessModules();
        foreach ($response_postprocess_modules AS $module) {
            $module->executePostprocess($response);
        }
    }

    /**
     * @param $hook_name
     * @param array $hook_data
     * @return HookResult
     * @throws \Exception
     */
    static function executeHook($hook_name, &$hook_data = array())
    {
        $result = new HookResult();

        if (!$hook_name) {
            throw new \Exception(__('Не указано имя хука'));
        }

        if ($hook_name != '*') {
            // при выполнении любого хука выполняется хук "*"
            self::executeHook('*',
                $tmp_arr = array(
                    'hook_name' => $hook_name,
                    'hook_data' => $hook_data
                ));
        }

        $hook_modules = self::getModulesByHookName($hook_name);
        foreach ($hook_modules AS $module) {
            try {
                $break_if_false = $module->executeHook($hook_name, $hook_data);
                $result->executed = true;
                if (false === $break_if_false) {
                    break;
                }
            } catch (\Exception $ex) {
                $result->addException($ex);
            }
        }
        return $result;
    }

    /**
     * @param $module
     * @return bool
     */
    static function isInstalled(Module $module)
    {
        return array_key_exists($module->getName(), Settings::getSettings('modules'));
    }


    /**
     * @param array $skip
     * @return Table[]
     * @throws \Exception
     */
    static protected function _getDbStructure($skip = array())
    {
        $skip = (array)$skip;
        $modules = self::getInstalled();
        /** @var Table[] $db_tables_common */
        $db_tables_common = array();
        foreach ($modules as $module) {
            if (in_array($module->getName(), $skip)) {
                continue;
            }

            /** @var Table[] $db_tables */
            $db_tables = $module->getDbStructure();
            foreach ($db_tables as $db_table) {
                $table_name = $db_table->getName();
                if (!array_key_exists($table_name, $db_tables_common)) {
                    $db_tables_common[$table_name] = new Table(array(
                        'name' => $table_name,
                        'fields' => array(),
                        'indexes' => array()
                    ));
                }

                $db_tables_common[$table_name]->addStructure($db_table);
            }
        }
        return $db_tables_common;
    }

    /**
     * @param Module $module
     * @throws \Exception
     */
    static function install(Module $module)
    {
        if (self::isInstalled($module)) {
            throw new \Exception(__('Модуль "%s" уже установлен', $module->getName()));
        }

        if (Cache::get('modules-installing', false)) {
            throw new \Exception(__('В данный момент устанавливается другой модуль'));
        }

        Cache::set('modules-installing', true, 60);

        try {
            $settings = Settings::getSettings('modules');
            $settings[$module->getName()] = $module->getConfig();

            try {
                Settings::setSettings('modules', $settings);
                $db_tables_current = self::_getDbStructure($module->getName());
                $db_tables_to = self::_getDbStructure();
                $rollback_sqls = array();

                try {
                    foreach ($db_tables_to as $db_table_to) {
                        if (array_key_exists($db_table_to->getName(), $db_tables_current)) {
                            $db_table_current = $db_tables_current[$db_table_to->getName()];
                            // запрос на модификацию таблицы
                            $sql = $db_table_current->getSqlQueryModifyTo($db_table_to);
                            $rollback_sql = $db_table_to->getSqlQueryModifyTo($db_table_current);
                        } else {
                            // запрос создание новой таблицы
                            $sql = $db_table_to->getSqlQueryCreate();
                            $rollback_sql = $db_table_to->getSqlQueryDrop();
                        }
                        if ($sql && !Db::me()->query($sql)) {
                            throw new \Exception(__('Не удалось выполнить SQL запрос "%s", "%s"', $sql,
                                print_r(Db::me()->errorInfo(), true)));
                        }
                        if ($rollback_sql) {
                            $rollback_sqls[] = $rollback_sql;
                        } // для каждого выполненного запроса сохраняем запрос для отката
                    }
                } catch (\Exception $e) {
                    // откатываем все изменения в базе данных при ошибке в одном из запросов
                    foreach ($rollback_sqls as $sql) {
                        Db::me()->quote($sql);
                    }
                    throw $e;
                }
            } catch (\Exception $ex) {
                // откат установки модуля
                unset($settings[$module->getName()]);
                Settings::setSettings('modules', $settings);
                throw $ex;
            }
        } catch (\Exception $e) {
            // отменяем установку
            Cache::set('modules-installing', false, 0);
            throw $e;
        }
        Cache::set('modules-installing', false, 0);
    }

    /**
     * @param Module $module
     * @throws \Exception
     */
    static function uninstall(Module $module)
    {
        if (!self::isInstalled($module)) {
            throw new \Exception(__('Модуль "%s" не установлен', $module->getName()));
        }
    }

    /**
     * @param Module $module
     * @throws \Exception
     */
    static function remove(Module $module)
    {
        self::uninstall($module);
    }

    /**
     * @param $name
     * @return Module
     * @throws \Exception
     */
    public static function getByName($name)
    {
        $modules = self::getAll();
        foreach ($modules AS $module) {
            if ($module->getName() === $name) {
                return $module;
            }
        }
        throw new \Exception(__('Модуль "%s" не найден', $name));
    }

    /**
     * @param Module $module
     * @return array
     * @throws \Exception
     */
    public static function getModuleDependencies(Module $module)
    {
        $dependencies_all = array();
        $dependencies_module = $module->getDependencies();
        foreach ($dependencies_module as $module_name) {
            try {
                $module_d = self::getByName($module_name);
            } catch (\Exception $ex) {
                throw new \Exception(__('Не найден зависимый модуль "%s" для модуля "%s"', $module_name,
                    $module->getName()));
            }

            $dependencies_all = array_merge($dependencies_all, self::getModuleDependencies($module_d));
        }
        return array_unique($dependencies_all);
    }
}