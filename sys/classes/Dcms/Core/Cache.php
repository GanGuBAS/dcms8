<?php

namespace Dcms\Core;

/**
 * Абстрактный класс для кэширования произвольных данных.
 */
abstract class Cache
{
    static private
        $_data = array(),
        $_data_ttl = array(); // rollback, если модуль кэширования не установлен

    /**
     * Получение данных из кэша
     * @param string $key
     * @param mixed $default
     * @return mixed
     */

    static public function get($key, $default = null)
    {
        if (array_key_exists($key, self::$_data) && array_key_exists($key,
                self::$_data_ttl) && self::$_data_ttl[$key] > time()
        ) {
            return self::$_data[$key];
        }

        $data = array(
            'key' => $key,
            'content' => $default
        );
        Modules::executeHook('Cache.get', $data);
        return $data['content'];
    }

    /**
     * Запись данных в кэш
     * @param string $key
     * @param mixed $content
     * @param int $ttl
     * @return boolean
     */
    static public function set($key, $content, $ttl = 0)
    {
        $data = array(
            'key' => $key,
            'content' => $content,
            'ttl' => $ttl
        );
        $executed = Modules::executeHook('Cache.set', $data)->executed;

        if (!$executed) { // rollback, если модуль кэширования не установлен
            self::$_data[$key] = $content;
            self::$_data_ttl[$key] = $ttl + time();
        }

        return true;
    }

    /**
     * Очистка кэша
     * @param string $key
     * @return boolean
     */
    static public function clear($key)
    {
        $data = array(
            'key' => $key
        );
        $executed = Modules::executeHook('Cache.clear', $data)->executed;

        if (!$executed) { // rollback, если модуль кэширования не установлен
            unset(self::$_data[$key], self::$_data_ttl[$key]);
        }
        return true;
    }
}