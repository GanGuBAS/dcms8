<?php

namespace Dcms\Core;

use Dcms\Models\Data\Module;

/**
 * Class controller
 * @property Module $module
 */
class Controller
{
    public
        $module;

    /**
     * @param $module \Dcms\Models\Data\Module
     */
    function __construct(Module $module)
    {
        $this->module = $module;
    }

    /**
     * Возвращает относительный путь к папке модуля.
     * Можно использовать для вывода в URL
     * @return string
     */
    function getCurrentPathRel()
    {
        return '/sys/classes/Modules/'.$this->module->getName();
    }

    /**
     * Возвращает абсолютный путь к папке модуля.
     * Используется в серверной части
     * @return string
     */
    function getCurrentPathAbs()
    {
        return H.'/sys/classes/Modules/'.$this->module->getName();
    }

    /**
     * Получение объекта для манипуляции с кэшем модуля
     * @return ControllerCache
     */
    function getCache()
    {
        return $this->module->getControllerCache();
    }

    /**
     * Получение объекта для манипуляции с настройками модуля
     * @return ControllerSettings
     */
    function getSettings()
    {
        return $this->module->getControllerSettings();
    }

    function __destruct()
    {
        unset($this->module);
    }
}