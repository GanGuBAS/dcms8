<?php

namespace Dcms\Core\Response;

class Error403Exception extends ErrorException
{

    function __construct()
    {
        parent::__construct(__("Доступ к данной странице запрещен"));
    }
}