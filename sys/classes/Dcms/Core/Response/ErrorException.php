<?php

namespace Dcms\Core\Response;

/**
 * Class ResponseException
 */
class ErrorException extends \Exception
{

    function __construct()
    {
        parent::__construct(__("Страница не может быть отображена"));
    }
}