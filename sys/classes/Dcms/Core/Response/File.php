<?php

namespace Dcms\Core\Response;

use Dcms\Misc\Charset;

/**
 * Отдача файла скриптом с поддержкой докачки. Используется для работы счетчиков скачиваний.
 */
class File
{
    public $path;
    public $name;
    public $mime = 'application/octet-stream';

    /**
     * @param string $path Абсолютный путь к файлу на сервере
     * @param string $name Название файла для отображения в браузере
     */
    public function __construct($path, $name)
    {
        $this->path = $path;
        $this->name = $name;
    }

    /**
     * Существует ли файл на сервере
     * @return bool
     */
    public function exists()
    {
        return is_file($this->path);
    }

    protected function _outputHeaders($size, $from, $to)
    {
        $is_range = !($from == 0 && $to == $size);
        if ($is_range) {
            header('HTTP/1.1 206 Partial Content');
        } else {
            header('HTTP/1.1 200 Ok');
        }
        $etag = md5($this->path);
        $etag = substr($etag, 0, 8).'-'.substr($etag, 8, 7).'-'.substr($etag, 15, 8);
        header('ETag: "'.$etag.'"');
        header('Accept-Ranges: bytes');
        header('Content-Length: '.($to - $from));
        if ($is_range) {
            header('Content-Range: bytes '.$from.'-'.$to.'/'.$size);
        }
        header('Connection: close');
        header('Last-Modified: '.gmdate('r', filemtime($this->path)));
        header('Last-Modified: '.gmdate('D, d M Y H:i:s', filemtime($this->path)).' GMT');
        header('Expires: '.gmdate('D, d M Y H:i:s', TIME + 3600).' GMT');
        if (!preg_match('#^image/#i', $this->mime)) {
            if ($this->name) {
                header('Content-Disposition: attachment; filename='.Charset::ofUtf8(basename($this->name)));
            } else {
                header('Content-Disposition: attachment; filename='.basename($this->path));
            }
        }
    }

    protected function _outputContent($from, $to)
    {
        if (@function_exists('ini_set')) {
            // при использовании встроенного сжатия возникали проблемы со скачиванием файлов
            ini_set('zlib.output_compression', 'Off');
        }

        $f = fopen($this->path, 'rb');
        fseek($f, $from, SEEK_SET);
        $size = $to;
        $downloaded = 0;
        while (!feof($f) and ! connection_status() and ( $downloaded < $size)) {
            $block = min(1024 * 8, $size - $downloaded);
            echo fread($f, $block);
            $downloaded += $block;
            flush();
        }
        fclose($f);
        return $downloaded; // возвращаем кол-во скачаных байт
    }

    /**
     * Отправляет запрошенное содержимое в браузер
     * @return int кол-во отправленных байт
     */
    public function output()
    {
        @ob_end_clean();
        $from = 0;

        $to = $size = filesize($this->path);
        if (isset($_SERVER['HTTP_RANGE'])) {
            if (preg_match('#bytes=-([0-9]+)#i', $_SERVER['HTTP_RANGE'], $range)) { // если указан отрезок от конца файла
                $from = $size - $range[1]; // начало файла
                $to = $size; // конец файла
            } elseif (preg_match('#bytes=([0-9]+)-#i', $_SERVER['HTTP_RANGE'], $range)) { // если указана только начальная метка
                $from = $range[1]; // начало
                $to = $size; // конец
            } elseif (preg_match('#bytes=([0-9]+)-([0-9]+)#i', $_SERVER['HTTP_RANGE'], $range)) { // если указан отрезок файла
                $from = $range[1]; // начало
                $to = $range[2]; // конец
            }
        }

        $this->_outputHeaders($size, $from, $to);
        return $this->_outputContent($from, $to);
    }
}