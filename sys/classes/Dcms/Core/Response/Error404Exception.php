<?php

namespace Dcms\Core\Response;

class Error404Exception extends ErrorException
{

    function __construct()
    {
        parent::__construct(__("Страница не найдена"));
    }
}