<?php

namespace Dcms\Core\Response;

use Dcms\Models\Ui\Page;
use Dcms\Core\Request\Request;
use Dcms\Models\Ui\Content\Items\Html;

/**
 * Ответ сервера
 * В зависимости от запроса, возвращает отрендеренный html код или json модели страницы.
 * Class response
 * @property Page $page
 */
class Response
{
    public
        $page,
        $last_modified;

    function __construct()
    {
        $this->page = new Page();
        $this->last_modified = time();
    }

    /**
     * Отдает в браузер файл.
     * @param string $path Путь к файлу на сервере
     * @param string $name Имя файла для сохранения у клиента
     * @throws Error404Exception
     */
    function file($path, $name = null)
    {
        $file = new File($path, $name);
        if (!$file->exists()) throw new Error404Exception();
        $file->output();
        exit;
    }

    /**
     */
    public function output()
    {
        header('Cache-Control: no-store, no-cache, must-revalidate', true);
        header('Expires: '.date('r'), true);
        header("Last-Modified: ".gmdate("D, d M Y H:i:s", $this->last_modified)." GMT", true);

        $this->page->meta->time_output = microtime(true);

        if ($content = ob_get_clean()) $this->page->content->items[] = new Html($content);

        switch (Request::getAccept()) {
            case Request::CTYPE_JSON:
                $this->_output_json();
                break;
            case Request::CTYPE_XML:
                $this->_output_xml();
                break;
            default:
                $this->_output_html();
                break;
        }
    }

    protected function _output_xml()
    {
        header('Content-Type: application/xml; charset=utf-8', true);

        echo $this->page->getXml();
    }

    protected function _output_json()
    {
        header('Content-Type: application/json; charset=utf-8', true);

        echo $this->page->getJson();
    }

    protected function _output_html()
    {
        header('X-UA-Compatible: IE=edge', true);
        header('Content-Type: text/html; charset=utf-8', true);

        echo $this->page->render('page.view');
    }
}