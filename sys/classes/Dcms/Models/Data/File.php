<?php

namespace Dcms\Models\Data;

class File
{
    public
        $rel_path = "",
        $name = "",
        $size = 0;
    protected
        $_abs_path;

    function __construct($abs_path)
    {
        $this->_abs_path = realpath($abs_path);
        if (!$this->_abs_path || !is_file($this->_abs_path)) {
            throw new \Exception(__('Файл %s не существует',  basename($abs_path)));
        }

        $this->name = basename($this->_abs_path);
        $this->size = filesize($this->_abs_path);
    }
}