<?php

namespace Dcms\Models\Data;

use Dcms\Misc\FileSystem;
use Dcms\Misc\Json;

class Theme
{
    protected $_abs_path;
    protected $_name;
    protected $_config;
    protected $_styles;
    protected $_scripts;

    function __construct($abs_path)
    {
        $this->_abs_path = $abs_path;
        $this->_loadConfig();
    }

    protected function _loadConfig()
    {
        if (false === ($path = realpath($this->_abs_path))) {
            throw new \Exception("Не удалось открыть тему оформления по указанному пути");
        }

        if (false === ($conf_path = realpath($path . '/config.json'))) {
            throw new \Exception("Не удалось найти конфиг темы оформления по указанному пути");
        }

        if (($config_json = @file_get_contents($conf_path)) === false) {
            throw new \Exception("Не удалось прочитать конфиг темы оформления");
        }

        $this->_name = basename($path);

        try {
            $this->_config = Json::decode($config_json);
        } catch (\Exception $e) {
            throw new \Exception("При чтении конфига темы оформления произошла ошибка: %s", $e->getMessage());
        }
    }

    /**
     * Получение параметра из конфига
     * @param $key
     * @param null $default
     * @return null
     */
    function getConfigProperty($key, $default = null)
    {
        if (!array_key_exists($key, $this->_config)) {
            return $default;
        }
        return $this->_config[$key];
    }

    /**
     * Получение списка зависимых модулей
     * @return array
     */
    function getDependencies()
    {
        return (array)$this->getConfigProperty('dependencies', array());
    }

    /**
     * Получение пути к папке темы для использования в браузере
     * @return string
     */
    function getThemePathOnClient()
    {
        return FileSystem::getRelPath($this->getThemePathOnServer());
    }

    /**
     * Получение пути к папке темы для использования на сервере
     * @return string
     */
    function getThemePathOnServer()
    {
        return $this->_abs_path;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->_name;
    }

    /**
     * @return string[]
     */
    public function getStyles()
    {
        if (is_null($this->_styles)) {
            $this->_styles = (array)$this->getConfigProperty('styles', array());
            foreach ($this->_styles AS $index => $path) {
                $this->_styles[$index] = '/' . $this->_replace_client_path($path);
            }
        }
        return $this->_styles;
    }

    /**
     * @return string[]
     */
    public function getScripts()
    {
        if (is_null($this->_scripts)) {
            $this->_scripts = (array)$this->getConfigProperty('scripts', array());
            foreach ($this->_scripts AS $index => $path) {
                $this->_scripts[$index] = '/' . $this->_replace_client_path($path);
            }
        }
        return $this->_scripts;
    }

    protected function _replace_client_path($path_with_variables)
    {
        static $map = null;
        if ($map === null) {
            $map = array(
                '{theme_path}' => $this->getThemePathOnClient()
            );
        }
        return str_replace(array_keys($map), array_values($map), $path_with_variables);
    }

    /**
     * Абсолютный путь к папке с вьюхами
     * @return string
     */
    public function getViewsPathAbs()
    {
        $views_path = $this->getConfigProperty('views_path', '{theme_path}');
        return str_replace('{theme_path}', $this->getThemePathOnServer(), $views_path);
    }

    public function getFavicon()
    {
        return $this->_replace_client_path($this->getConfigProperty('favicon', '/favicon.ico'));
    }
}