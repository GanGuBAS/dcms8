<?php

namespace Dcms\Models\Data\DatabaseStructure;

class Field
{
    public
        $type,
        $options = "",
        $name = "",
        $unsigned = false,
        $zerofill = false,
        $null = false,
        $default = "",
        $autoincrement = false;

    function __construct($field_config)
    {
        $this->name = $this->_parseName($field_config);
        $this->type = $this->_parseType($field_config);
        $this->options = $this->_parseOptions($field_config);
        $this->zerofill = $this->_parseZerofill($field_config);
        $this->unsigned = $this->_parseUnsigned($field_config);
        $this->null = $this->_parseNull($field_config);
        $this->default = $this->_parseDefault($field_config);
        $this->autoincrement = $this->_parseAI($field_config);
    }

    function getSqlCreateDefinition()
    {
        $str_arr = array('`' . $this->name . '`');

        $str_arr[] = $this->type . ($this->options ? '(' . $this->options . ')' : '');

        //if ($this->unsigned)
        //    $str_arr[] = 'UNSIGNED';
        if ($this->zerofill) {
            $str_arr[] = 'ZEROFILL';
        }

        $str_arr[] = ($this->null ? '' : 'NOT ') . 'NULL';

        if ($this->default) {
            $str_arr[] = "DEFAULT '" . $this->default . "'";
        }

        //if ($this->autoincrement)
        //    $str_arr[] = "AUTOINCREMENT";

        return join(' ', $str_arr);
    }

    public function getSqlQueryModifyTo(Field $field)
    {
        if ($this->getName() !== $field->getName()) {
            throw new \Exception(__("Имена полей не совпадают"));
        }

        $sql_current = $this->getSqlCreateDefinition();
        $sql_to = $field->getSqlCreateDefinition();

        if ($sql_current === $sql_to) {
            return null;
        }

        return 'CHANGE `' . $this->name . '` ' . $sql_to;
    }

    public function getSqlDropDefinition()
    {
        return 'DROP `' . $this->getName() . '`';
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    protected function _parseType($field_config)
    {
        if (!array_key_exists('type', $field_config)) {
            return 'TEXT';
        }
        return $field_config['type'];
    }

    protected function _parseZerofill($field_config)
    {
        if (!array_key_exists('zerofill', $field_config) || !$field_config['zerofill']) {
            return false;
        }
        return true;
    }

    protected function _parseUnsigned($field_config)
    {
        if (!array_key_exists('unsigned', $field_config) || !$field_config['unsigned']) {
            return false;
        }
        return true;
    }

    protected function _parseNull($field_config)
    {
        if (!array_key_exists('null', $field_config) || !$field_config['null']) {
            return false;
        }
        return true;
    }

    protected function _parseDefault($field_config)
    {
        if (!array_key_exists('default', $field_config)) {
            return '';
        }
        return $field_config['default'];
    }

    protected function _parseAI($field_config)
    {
        if (!array_key_exists('autoincrement', $field_config) || !$field_config['autoincrement']) {
            return false;
        }
        return true;
    }

    protected function _parseName($field_config)
    {
        if (!array_key_exists('name', $field_config) || !$field_config['name']) {
            throw new \Exception(__("Имя поля не может быть пустым"));
        }
        return $field_config['name'];
    }

    public function _parseOptions($field_config)
    {
        if (!array_key_exists('options', $field_config)) {
            return '';
        }
        return $field_config['options'];
    }
}