<?php

namespace Dcms\Models\Data\DatabaseStructure;

/**
 * Class Index
 * @property \string[] $fields
 */
class Index
{
    public
        $name = "",
        $unique = false,
        $primary = false,
        $fulltext = false,
        $fields = array();

    function __construct($index_config)
    {
        $this->name = $this->_parseName($index_config);
        $this->unique = $this->_parseUnique($index_config);
        $this->primary = $this->_parsePrimary($index_config);
        $this->fulltext = $this->_parseFulltext($index_config);
        $this->fields = $this->_parseFields($index_config);
    }

    function getName()
    {
        if (!$this->name) {
            foreach ($this->fields as $field_name) {
                $this->name .= $field_name;
            }
        }
        return $this->name;
    }

    function getSqlCreateIndexDefinition($with_fields = true)
    {
        $str_arr = array();
        if ($this->primary) {
            $str_arr[] = 'PRIMARY KEY';
        } else {
            if ($this->unique) {
                $str_arr[] = 'UNIQUE `' . $this->name . '`';
            } else {
                if ($this->fulltext) {
                    $str_arr[] = 'FULLTEXT `' . $this->name . '`';
                } else {
                    $str_arr[] = 'KEY `' . $this->name . '`';
                }
            }
        }

        if ($with_fields) {
            $field_names = array();
            foreach ($this->fields AS $field_name) {
                $field_names[] = '`' . $field_name . '`';
            }

            $str_arr[] = '(' . join(',', $field_names) . ')';
        }
        return join(' ', $str_arr);
    }

    public function _parseName($index_config)
    {
        if (!array_key_exists('name', $index_config) || !$index_config['name']) {
            throw new \Exception(__("Имя индекса не может быть пустым"));
        }
        return $index_config['name'];
    }

    public function _parseUnique($index_config)
    {
        if (!array_key_exists('unique', $index_config) || !$index_config['unique']) {
            return false;
        }
        return true;
    }

    public function _parsePrimary($index_config)
    {
        if (!array_key_exists('primary', $index_config) || !$index_config['primary']) {
            return false;
        }
        return true;
    }

    public function _parseFulltext($index_config)
    {
        if (!array_key_exists('fulltext', $index_config) || !$index_config['fulltext']) {
            return false;
        }
        return true;
    }

    public function _parseFields($index_config)
    {
        if (!array_key_exists('fields', $index_config) || !$index_config['fields']
            || !is_array($index_config['fields'])
        ) {
            return array($this->getName());
        }
        return $index_config['fields'];
    }
}