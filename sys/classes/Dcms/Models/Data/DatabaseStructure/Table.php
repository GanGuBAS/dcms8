<?php

namespace Dcms\Models\Data\DatabaseStructure;

/**
 * Class Table
 * @property  Field[] fields
 * @property Index[] indexes
 */
class Table
{
    public
        $name = "",
        $fields = array(),
        $indexes = array();

    /**
     * @param $table_config
     * @throws \Exception
     */
    function __construct($table_config)
    {
        if (!is_array($table_config)) {
            throw new \Exception(__('Параметры таблицы должны быть переданы в виде ассоциативного массива'));
        }

        if (!array_key_exists('name', $table_config) || !$table_config['name']) {
            throw new \Exception(__('Не указано название таблицы'));
        }

        $this->name = (string)$table_config['name'];

        if (!array_key_exists('fields', $table_config) || !is_array($table_config['fields'])) {
            throw new \Exception(__('Не указан список полей таблицы'));
        }

        foreach ($table_config['fields'] AS $field_config) {
            $this->fields[] = new Field($field_config);
        }

        if (!array_key_exists('indexes', $table_config) || !is_array($table_config['indexes'])) {
            throw new \Exception(__('Не указан список индексов таблицы'));
        }

        foreach ($table_config['indexes'] AS $index_config) {
            $this->indexes[] = new Index($index_config);
        }
    }

    /**
     *
     * @param string $name
     * @return Field
     */
    public function getFieldByName($name)
    {
        foreach ($this->fields as $field) {
            if ($field->getName() === $name) {
                return $field;
            }
        }
    }

    /**
     *
     * @param string $name
     * @return Index
     */
    public function getIndexByName($name)
    {
        foreach ($this->indexes as $index) {
            if ($index->getName() === $name) {
                return $index;
            }
        }
    }

    /**
     * Добавляет поля и индексы из переданной структуры
     * @param Table $struct
     * @throws \Exception
     * @return $this
     */
    function addStructure(Table $struct)
    {
        if ($this->getName() !== $struct->getName()) {
            throw new \Exception(__("Имена таблиц не совпадают"));
        }

        foreach ($struct->fields as $field) {
            $curr_field = $this->getFieldByName($field->getName());
            if (!$curr_field) {
                $this->fields[] = $field;
            } else {
                if ($curr_field->getSqlCreateDefinition() !== $field->getSqlCreateDefinition()) {
                    throw new \Exception(__('В таблице "%s" уже существует поле "%s" с другой структурой',
                        $this->name, $field->getName()));
                }
            }
        }

        foreach ($struct->indexes as $index) {
            $curr_index = $this->getIndexByName($index->getName());
            if (!$curr_index) {
                $this->indexes[] = $index;
            } else {
                if ($curr_index->getSqlCreateIndexDefinition() !== $index->getSqlCreateIndexDefinition()) {
                    throw new \Exception(__('В таблице "$s" уже существует индекс "%s" с другой структурой',
                        $this->name, $index->getName()));
                }
            }
        }
        return $this;
    }

    /**
     * Возвращает SQL запрос на создание таблицы из текущей структуры
     * @return string
     */
    function getSqlQueryCreate()
    {
        $str_arr = array();

        foreach ($this->fields as $field) {
            $str_arr[] = $field->getSqlCreateDefinition();
        }

        foreach ($this->indexes as $index) {
            $str_arr[] = $index->getSqlCreateIndexDefinition();
        }

        return 'CREATE TABLE IF NOT EXISTS `' . $this->getName() . '` (' . implode(', ',
            $str_arr) . ')';
    }

    /**
     * Возвращает SQL запрос на модификацию таблицы к переданной структуре
     * @param Table $struct
     * @throws \Exception
     * @return string SQL-запрос
     */
    function getSqlQueryModifyTo(Table $struct)
    {
        $str_arr = array('ALTER TABLE `' . $this->getName() . '`');
        $str_struct_arr = array();

        if ($this->getName() !== $struct->getName()) {
            throw new \Exception(__("Имена таблиц не совпадают"));
        }

        // удаление индексов
        foreach ($this->indexes as $index) {
            if (!$struct->getIndexByName($index->getName())) {
                $str_struct_arr[] = 'DROP ' . $index->getSqlCreateIndexDefinition(false);
            }
        }

        // удаление полей
        foreach ($this->fields as $field) {
            if (!$struct->getFieldByName($field->getName())) {
                $str_struct_arr[] = $field->getSqlDropDefinition();
            }
        }

        // удаление измененных индексов, которые нужно будет пересоздать
        foreach ($struct->indexes as $index) {
            $curr_index = $this->getIndexByName($index->getName());
            if ($curr_index) {
                $curr_index_def = $curr_index->getSqlCreateIndexDefinition();
                $index_def = $index->getSqlCreateIndexDefinition();
                if ($curr_index_def !== $index_def) {
                    $str_struct_arr[] = 'DROP ' . $curr_index->getSqlCreateIndexDefinition(false);
                }
            }
        }

        // добавление и модификация полей
        foreach ($struct->fields as $field) {
            $curr_field = $this->getFieldByName($field->getName());
            if (!$curr_field) {
                $str_struct_arr[] = $field->getSqlCreateDefinition();
            } else {
                $sql_modify_field = $curr_field->getSqlQueryModifyTo($field);
                if ($sql_modify_field) {
                    $str_struct_arr[] = $sql_modify_field;
                }
            }
        }

        // создание  индексов
        foreach ($struct->indexes as $index) {
            $curr_index = $this->getIndexByName($index->getName());
            if (!$curr_index) {
                $str_struct_arr[] = 'ADD' . $index->getSqlCreateIndexDefinition();
            } else {
                $curr_index_def = $curr_index->getSqlCreateIndexDefinition();
                $index_def = $index->getSqlCreateIndexDefinition();
                if ($curr_index_def !== $index_def) {
                    // создание удаленных ранее измененных индексов
                    $str_struct_arr[] = 'ADD' . $index->getSqlCreateIndexDefinition();
                }
            }
        }

        if (count($str_struct_arr) == 0) {
            return null;
        }

        $str_arr[] = implode(', ', $str_struct_arr);
        return implode(' ', $str_arr);
    }

    public function getName()
    {
        return $this->name;
    }

    public function getSqlQueryDrop()
    {
        return 'DROP TABLE `' . $this->getName() . '`';
    }
}