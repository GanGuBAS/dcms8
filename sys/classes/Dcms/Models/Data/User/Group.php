<?php

namespace Dcms\Models\Data\User;

/**
 * Class Group Группа пользователей
 * @property Permission[] $permissions
 */
class Group
{
    protected $_SYSTEM_PERMISSIONS = array(
        'guest' => array(
            'dependencies' => array(),
            'permissions' => array(
                'sys.site_view',
                'sys.registration',
                'sys.authorization'
            )
        ),
        'user' => array(
            'dependencies' => array(
                'guest'
            ),
            'permissions' => array(
            )
        ),
        'admin' => array(
            'dependencies' => array(
                'user'
            ),
            'permissions' => array(
            )
        )
    );
    protected
        $_is_system = false;
    public
        $key          = "",
        $name         = "",
        $permissions  = array(),
        $dependencies = array();

    function __construct($key, $is_system = false)
    {
        $this->_is_system = !!$is_system;
        $this->key        = $key;
        $this->_system_permissions();
        $this->_load_permissions();
    }

    protected function _load_permissions()
    {

    }

    /**
     * создание системных (не редактируемых) разрешений
     */
    private function _system_permissions()
    {
        switch ($this->key) {
            case 'guest':

                break;
            case 'user':

                break;
            case 'admin':

                break;
        }
    }
}