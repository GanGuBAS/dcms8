<?php

namespace Dcms\Models\Data;

class HookResult
{
    public
        $executed = false,
        $exceptions = array();

    /**
     * 
     * @param \Exception $ex
     */
    public function addException($ex)
    {
        $this->exceptions[] = $ex;
    }
}