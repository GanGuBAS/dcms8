<?php

namespace Dcms\Models\Ui\Messages;

use Dcms\Core\UiModelRender;

/**
 * Сообщение
 * Class Item
 * @property string text
 * @property bool is_error
 */
class Item extends UiModelRender
{
    public
        $text,
        $is_error;

    /**
     * @param $text
     * @param bool $is_error
     */
    function __construct($text, $is_error = false)
    {
        parent::__construct();
        $this->text     = $text;
        $this->is_error = $is_error;
    }
}