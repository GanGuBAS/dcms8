<?php
namespace Dcms\Models\Ui\Messages;

use Dcms\Core\UiModelRender;

/**
 * Список сообщений
 * Class Messages
 * @property Item[] items
 */
class Messages extends UiModelRender
{
    public $items = array();

    public function err($text)
    {
        return $this->items[] = new Item($text, true);
    }

    public function msg($text)
    {
        return $this->items[] = new Item($text, false);
    }
}