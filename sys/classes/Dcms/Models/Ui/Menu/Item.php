<?php

namespace Dcms\Models\Ui\Menu;

use Dcms\Core\UiModelRender;
use Dcms\Core\Url;

/**
 * Class Item
 * @package Dcms\Models\Ui\Menu
 */
class Item extends UiModelRender
{
    public
        $url,
        $name,
        $icon,
        $is_active  = false,
        $menu_items = array();

    /**
     * Установка имени
     * @param $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Установка пути
     * @param string|Url $url
     */
    public function setUrl($url)
    {
        $this->url = (string) new Url($url);
    }
}