<?php
namespace Dcms\Models\Ui\Menu;

use Dcms\Core\UiModelRender;
use Dcms\Core\Url;

/**
 * Class Menu
 * @package Dcms\Models\Ui\Menu
 */
class Menu extends UiModelRender
{
    public $items = array();

    /**
     * @param string $name
     * @param string|Url $url
     * @param bool $is_active
     * @return Item
     */
    public function addItem($name, $url, $is_active = false)
    {
        $this->items[] = $item = new Item();
        $item->setName($name);
        $item->setUrl($url);
        $item->is_active = $is_active;
        return $item;
    }
}