<?php

namespace Dcms\Models\Ui\Meta;

use Dcms\Core\System;
use Dcms\Core\ThemeRender;
use Dcms\Core\Modules;
use Dcms\Core\UiModelRender;

/**
 * Метаданные страницы
 * Class Meta
 * @property string title
 * @property string[] keywords
 * @property string description
 * @property string[] styles
 * @property string[] scripts
 */
class Meta extends UiModelRender
{
    protected $_render;
    public
        $title,
        $keywords,
        $description,
        $generator,
        $lang,
        $ajax_timeout = 7,
        $ajax_timeout_error = 30,
        $styles = array(),
        $scripts = array(),
        $favicon = '/favicon.ico';
    public $time_start, $time_output;

    public function __construct()
    {
        parent::__construct();
        $this->time_start = microtime(true);
        $this->generator = 'DCMS ' . System::getProperty('version');

        $this->title = System::getProperty('title', __('[Заголовок не задан]'));
        $this->_render = ThemeRender::getInstance();
        $modules = array();
        $dependencies = (array)$this->_render->getTheme()->getDependencies();
        if (count($dependencies)) {
            $modules = Modules::getInstalled();
        }
        foreach ($dependencies as $dependence) {
            foreach ($modules as $module) {
                if (in_array($dependence, $module->getThemeDependencies())) {
                    $module_controller_instance = $module->getControllerInstance();
                    $module_dependence_handler = $module->getThemeDependenceHandler($dependence);
                    $module_controller_instance->$module_dependence_handler($this);
                    break;
                }
            }
        }

        $this->favicon = $this->_render->getTheme()->getFavicon();
        $this->styles = array_merge($this->styles,
            $this->_render->getTheme()->getStyles());
        $this->scripts = array_merge($this->scripts,
            $this->_render->getTheme()->getScripts());
    }
}