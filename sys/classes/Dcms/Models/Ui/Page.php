<?php

namespace Dcms\Models\Ui;

use Dcms\Core\UiModelRender;
use Dcms\Misc\Json;
use Dcms\Core\Request\Request;
use Dcms\Misc\Misc;

/**
 * Модель страницы сайта
 * Class Page
 * @property Meta\Meta meta Дополнительные параметры страницы
 * @property Breadcrumbs\Breadcrumbs $breadcrumbs
 * @property Tabs\Tabs tabs
 * @property Messages\Messages messages
 * @property Menu\Menu menu
 * @property Content\Content content
 */
class Page extends UiModelRender
{
    public
        $meta,
        $breadcrumbs,
        $tabs,
        $messages,
        $menu,
        $content,
        $user_menu,
        $user;

    public function __construct()
    {
        parent::__construct();
        $this->meta = new Meta\Meta();
        $this->breadcrumbs = new Breadcrumbs\Breadcrumbs();
        $this->tabs = new Tabs\Tabs();
        $this->messages = new Messages\Messages();
        $this->menu = new Menu\Menu();
        $this->content = new Content\Content();
        $this->user_menu = new User\Menu();
    }

    /**
     * @param $content
     * @return Content\Items\Html
     */
    public function addContentHtml($content)
    {
        return $this->content->items[] = new Content\Items\Html($content);
    }

    /**
     * @param $text
     * @return Messages\Item
     */
    public function err($text)
    {
        return $this->messages->err($text);
    }

    /**
     * @param $text
     * @return Messages\Item
     */
    public function msg($text)
    {
        return $this->messages->msg($text);
    }

    /**
     * Установка заголовка страницы
     * @param string $string
     */
    public function setTitle($string)
    {
        $this->meta->title = $string;
    }

    /**
     * @return Menu\Menu
     */
    public function getMenu()
    {
        return $this->menu;
    }

    /**
     * @return Tabs\Tabs
     */
    public function getTabs()
    {
        return $this->tabs;
    }

    /**
     * @return Breadcrumbs\Breadcrumbs
     */
    public function getBreadcrumbs()
    {
        return $this->breadcrumbs;
    }

    /**
     * @return Content\Content
     */
    public function getContent()
    {
        return $this->content;
    }

    public function getData()
    {
        $model_key = Request::getModelKey();
        if ($model_key) {
            $client_model = $this->_getModel($model_key);
            $diff_model_array = $this->_getDiffModel(Json::decode(Json::encode($this)), $client_model);
        }
        return array(
            'model' => isset($diff_model_array) ? $diff_model_array : Json::decode(Json::encode($this)),
            'model_key' => $this->_saveCurrentModel()
        );
    }

    public function getJson()
    {
        $model_key = Request::getModelKey();
        if ($model_key) {
            $client_model = $this->_getModel($model_key);
            $diff_model_array = $this->_getDiffModel(Json::decode(Json::encode($this)), $client_model);
        }
        return Json::encode(array(
            'model' => isset($diff_model_array) ? $diff_model_array : $this,
            'model_key' => $this->_saveCurrentModel()
        ));
    }

    public function getXml()
    {
        $a2x = new \Array2XML();
        $xml = $a2x->createXML('root', $this->getData());
        return $xml->saveXML();
    }

    /**
     * Сохранение текущей модели страницы.
     * Необходимо, чтобы при последующем запросе мы могли отправить только измененные данные.
     * @return string
     * @throws \Exception
     */
    private function _saveCurrentModel()
    {
        $model_key = Misc::getRandomPhrase();

        if (!isset($_SESSION['model_hashes'])) {
            $_SESSION['model_hashes'] = array();
        }

        foreach ($_SESSION['model_hashes'] as $session_model_key => $session_model_hash) {
            if ($session_model_hash['time'] < TIME - 60) {
                unset($_SESSION['model_hashes'][$session_model_key]);
            }
        }

        $_SESSION['model_hashes'][$model_key] = array('time' => TIME, 'data' => Json::decode(Json::encode($this)));

        return $model_key;
    }

    /**
     * Возвращает текущую модель на клиенте по ее ключу
     * @param $model_key
     * @return null|array
     */
    private function _getModel($model_key)
    {
        if (!isset($_SESSION['model_hashes'])) {
            return null;
        }
        if (!isset($_SESSION['model_hashes'][$model_key])) {
            return null;
        }
        return $_SESSION['model_hashes'][$model_key]['data'];
    }

    /**
     * Возвращает разницу в моделях страницы для модификации на клиенте
     * @param array $current_model
     * @param array $client_model
     * @return array
     */
    private function _getDiffModel($current_model, $client_model)
    {
        $assoc_array = array();

        foreach ($current_model as $current_model_key => $current_model_value) {

            // скалярное
            if (is_scalar($current_model_value) || is_null($current_model_value)) {
                if (!array_key_exists($current_model_key,
                        $client_model) || $client_model[$current_model_key] !== $current_model_value
                ) {
                    $assoc_array[$current_model_key] = $current_model_value;
                }
                continue;
            }

            // объект или ассоциативный массив
            if (is_array($current_model_value) && misc::isAssocArray($current_model_value)) {
                if (array_key_exists($current_model_key, $client_model) && is_array($client_model[$current_model_key])
                ) {
                    $assoc_result = $this->_getDiffModel($current_model_value, $client_model[$current_model_key]);
                } else {
                    $assoc_result = $current_model_value;
                }

                if ($assoc_result) {
                    $assoc_array[$current_model_key] = $assoc_result;
                }
                continue;
            }

            // секвентальный массив
            if (is_array($current_model_value) && !misc::isAssocArray($current_model_value)) {
                if (count($current_model_value) === 0) {
                    if (array_key_exists($current_model_key, $client_model) && $client_model[$current_model_key]) {
                        $assoc_array[$current_model_key] = array();
                    }
                } else {
                    if (!array_key_exists($current_model_key,
                            $client_model) || !is_array($client_model[$current_model_key])
                        || count($client_model[$current_model_key]) === 0
                    ) {
                        $assoc_array[$current_model_key] = $current_model_value;
                    } else {
                        $need_modify = false;
                        $modify = array(
                            'LENGTH' => count($current_model_value), // длина списка
                            'MERGE' => array(), // дополняем или заменяем содержимое у элементов списка
                            'REPLACE' => array(), // заменяем элементы списка
                            'MOVE' => array() // перемещаем элементы списка
                        );

                        $client_model_value = (array)$client_model[$current_model_key];
                        $client_model_hashes = array();
                        for ($i = 0; $i < count($client_model_value); $i++) {
                            $client_model_hashes[] = md5(Json::encode($client_model_value[$i]));
                        }

                        $current_model_hashes = array();
                        for ($i = 0; $i < count($current_model_value); $i++) {
                            $current_model_hashes[] = md5(Json::encode($current_model_value[$i]));
                        }


                        for ($i = 0; $i < count($current_model_hashes); $i++) {

                            if (array_key_exists($i,
                                    $client_model_hashes) && $client_model_hashes[$i] === $current_model_hashes[$i]
                            ) {
                                // совпадение по ключу и содержимому
                                continue;
                            }

                            if (in_array($current_model_hashes[$i], $client_model_hashes)) {
                                // есть на клиенте, но в другой позиции
                                $need_modify = true;
                                $modify['MOVE'][] = array(
                                    'f' => array_search($current_model_hashes[$i], $client_model_hashes),
                                    't' => $i
                                );
                                continue;
                            }

                            if (is_array($current_model_value[$i]) && misc::isAssocArray($current_model_value[$i])) {
                                // элемент в списке является ассоциативным массивом (объектом)
                                if (!array_key_exists($i,
                                        $client_model_hashes) || !is_array($client_model_value[$i]) || !misc::isAssocArray($client_model_value[$i])
                                ) {
                                    // на клиенте этого элемента нет или он не является объектом
                                    $need_modify = true;
                                    $modify['REPLACE'][] = array(
                                        'i' => $i,
                                        'c' => $current_model_value[$i]
                                    );
                                    continue;
                                } else {
                                    $need_modify = true;
                                    $modify['MERGE'][] = array(
                                        'i' => $i,
                                        'c' => $this->_getDiffModel($current_model_value[$i], $client_model_value[$i])
                                    );
                                }
                            }
                        }

                        if ($need_modify) {
                            $assoc_array[$current_model_key] = array('@MODIFY' => $modify);
                        }
                    }
                }

                continue;
            }
        }

        foreach ($client_model AS $client_model_key => $client_model_value) {
            if (!array_key_exists($client_model_key, $current_model)) {
                $assoc_array[$client_model_key] = null;
            }
        }

        return $assoc_array;
    }
}