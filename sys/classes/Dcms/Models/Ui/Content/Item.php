<?php

namespace Dcms\Models\Ui\Content;

use Dcms\Core\UiModelRender;

/**
 * Блок с содержимым страницы
 * Class page_content_item
 * @property Item[] content_items
 */
class Item extends UiModelRender
{
    public $type          = 'default';
    public $content_items = array();

}