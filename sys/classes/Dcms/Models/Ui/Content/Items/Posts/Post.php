<?php

namespace Dcms\Models\Ui\Content\Items\Posts;

use Dcms\Core\Modules;
use Dcms\Models\Ui\Content\Item;
use Dcms\Models\Ui\Content\Items\Posts\Actions\Actions;

/**
 * Class page_post
 * @property boolean highlight
 * @property string image_src
 * @property string icon_class
 * @property string title
 * @property string url
 * @property Actions actions
 * @property string content_html
 * @property string bottom_html
 * @property Time time
 * @property string counter
 */
class Post extends Item
{
    public $type          = 'post';
    public
        $highlight     = false,
        $image_html    = "",
        $icon_html     = "",
        $title         = "",
        $url           = "",
        $actions,
        $content_html  = "",
        $bottom_html   = "",
        $time,
        $counter       = "";
    protected
        $_image_src = "",
        $_icon_src  = "";

    function __construct()
    {
        parent::__construct();
        $this->actions = new Actions();
        $this->time    = new Time();
    }

    public function getIconSrc()
    {
        return $this->_icon_src;
    }

    /**
     * @param string $rel_path
     */
    public function setIconSrc($rel_path, $replace_image = true)
    {
        $this->_icon_src = $rel_path;
        if ($this->_icon_src) {
            $this->icon_html = '<img src="'.htmlspecialchars($rel_path).'" alt="'.htmlspecialchars(basename($rel_path)).'" />';

            if ($replace_image) {
                $this->image_html = "";
                $this->_image_src = "";
            }
        } else {
            $this->icon_html = "";
        }
        Modules::executeHook('page_post.setIconSrc', $this);
    }

    public function getImageSrc()
    {
        return $this->_image_src;
    }

    /**
     * @param string $rel_path
     */
    public function setImageSrc($rel_path, $replace_icon = true)
    {
        $this->_image_src = $rel_path;
        if ($this->_image_src) {
            $this->image_html = '<img src="'.htmlspecialchars($rel_path).'" alt="'.htmlspecialchars(basename($rel_path)).'" />';
            if ($replace_icon) {
                $this->icon_html = "";
                $this->_icon_src = "";
            }
        } else {
            $this->image_html = "";
        }
        Modules::executeHook('page_post.setImageSrc', $this);
    }

    /**
     * @param int|\Time $time
     */
    public function setTime($time)
    {
        if ($time instanceof Time) {
            $this->time = $time;
        } else {
            $this->time->timestamp = (int) $time;
            $this->time->show      = true;
        }
    }

    /**
     * @param string $str
     */
    public function setTitle($str)
    {
        $this->title = (string) $str;
    }

    /**
     * @param string $content_html
     */
    public function setContentHtml($content_html)
    {
        $this->content_html = (string) $content_html;
    }

    /**
     * @param string $bottom_html
     */
    public function setBottomHtml($bottom_html)
    {
        $this->bottom_html = (string) $bottom_html;
    }

    /**
     * @return Post
     */
    public function addPost()
    {
        return $this->content_items[] = new Post();
    }

    /**
     * @param string|\Dcms\Core\Url $url
     */
    public function setUrl($url)
    {
        $this->url = (string) new \Dcms\Core\Url($url);
    }
}