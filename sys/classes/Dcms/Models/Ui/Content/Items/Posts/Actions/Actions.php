<?php

namespace Dcms\Models\Ui\Content\Items\Posts\Actions;

use Dcms\Core\UiModelRender;
use Dcms\Core\Url;

/**
 * Список возможных действий надо постом
 * Class Actions
 * @property Item[] items
 */
class Actions extends UiModelRender
{
    public $items = array();

    /**
     * @param string $name
     * @param string|Url $url
     * @return Item
     */
    public function addItem($name, $url)
    {
        $this->items[] = $item = new Item();
        $item->name = $name;
        $item->setUrl($url);
        return $item;
    }
}