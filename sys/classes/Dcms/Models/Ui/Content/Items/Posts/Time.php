<?php

namespace Dcms\Models\Ui\Content\Items\Posts;

use Dcms\Core\UiModelRender;

/**
 * Class Time
 * @property int $timestamp
 * @property bool $show
 * @property string $format
 */
class Time extends UiModelRender
{
    public
        $show   = false,
        $timestamp,
        $format = 'd MMM yy H:mm:ss';

}