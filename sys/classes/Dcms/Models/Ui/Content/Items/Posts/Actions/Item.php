<?php

namespace Dcms\Models\Ui\Content\Items\Posts\Actions;

use Dcms\Core\UiModelRender;
use Dcms\Core\Url;

/**
 * Class Item
 * @property string $name
 * @property string $url
 */
class Item extends UiModelRender
{
    public
        $name = "",
        $url = "";

    public function setUrl($url)
    {
        $this->url = (string) new Url($url);
    }
}