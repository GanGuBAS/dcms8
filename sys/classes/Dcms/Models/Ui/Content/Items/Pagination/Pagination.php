<?php

namespace Dcms\Models\Ui\Content\Items\Pagination;

use Dcms\Models\Ui\Content\Item;

/**
 * Постраничная навигация
 * Class page_pagination
 */
class Pagination extends Item
{
    public $type = 'pagination';
    public
        $count,
        $current,
        $url_template;
    protected $_items_per_page = 10;
    protected $_items_count = 0;

    /**
     * @param int $items_count
     */
    function __construct($items_count = 0)
    {
        parent::__construct();
        //$user = current_user::getInstance();
        //$this->_items_per_page = $user->items_per_page;
        $this->_items_count = $items_count;
        $this->_reCalc();
    }

    /**
     *
     */
    protected function _reCalc()
    {
        if (!$this->_items_count) {
            $this->count = 1;
        } else {
            $this->count = ceil($this->_items_count / $this->_items_per_page);
        }

        if (isset($_GET['page'])) {
            if ($_GET['page'] == 'end') {
                $this->current = $this->count;
            } elseif (is_numeric($_GET['page'])) {
                $this->current = max(1, min($this->count, intval($_GET['page'])));
            } else {
                $this->current = 1;
            }
        } elseif (isset($_GET['postnum'])) {
            if ($_GET['postnum'] == 'end') {
                $this->current = $this->count;
            } elseif (is_numeric($_GET['postnum'])) {
                $this->current = max(1,
                    min($this->count,
                        ceil($_GET['postnum'] / $this->_items_per_page)));
            } else {
                $this->current = 1;
            }
        } else {
            $this->current = 1;
        }
    }

    /**
     * @param int $items_per_page
     */
    public function setItemsPerPage($items_per_page)
    {
        $this->_items_per_page = $items_per_page;
        $this->_reCalc();
    }

    /**
     * @param mixed $current
     */
    public function setCurrent($current)
    {
        $this->current = $current;
        $this->_reCalc();
    }
}