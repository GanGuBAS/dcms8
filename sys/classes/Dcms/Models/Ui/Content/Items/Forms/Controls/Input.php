<?php
namespace Dcms\Models\Ui\Content\Items\Forms\Controls;

use Dcms\Models\Ui\Content\Items\Forms\Control;

/**
 * input
 * Class Input
 */
class Input extends Control
{
    public $type       = 'input';
    public $input_type = 'text';
    public $value      = '';
    public $checked  = false;
    public $disabled = false;

    /**
     * Установка значения, которое приходит из вне
     * TODO: запилить паттерны и сверять значение с ними
     * @param $value
     */
    public function setValue($value)
    {
        $this->value = $value;
    }
}