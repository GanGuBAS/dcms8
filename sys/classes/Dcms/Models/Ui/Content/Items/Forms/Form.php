<?php
namespace Dcms\Models\Ui\Content\Items\Forms;

use Dcms\Core\Request\Request;
use Dcms\Core\Url;
use Dcms\Models\Ui\Content\Item;
use Dcms\Models\Ui\Content\Items\Forms\Controls\Input;
use Dcms\Models\Ui\Content\Items\Forms\Controls\Textarea;

/**
 * Форма
 * Class page_form
 * @property Control[] items
 */
class Form extends Item
{
    public $type = 'form';
    public $action = '';
    public $method = 'post';
    public $items = array();

    function __construct($url = null)
    {
        parent::__construct();
        $this->setUrl($url);
    }

    public function setUrl($url)
    {
        $this->action = (string)(is_null($url) ? Request::getUrl() : new Url($url));
    }

    /**
     * @param $title
     * @param string $name
     * @return Input
     */
    public function addButton($title, $name = '')
    {
        $this->items[] = $button = new Input();
        $button->input_type = 'submit';
        $button->value = $title;
        $button->name = $name;
        return $button;
    }

    /**
     * @param $title
     * @param $name
     * @param string $value
     * @return Input
     */
    public function addTextField($title, $name, $value = '')
    {
        $this->items[] = $input = new Input();
        $input->input_type = 'text';
        $input->name = $name;
        $input->title = $title;
        $input->value = $value;
        return $input;
    }

    /**
     * @param $title
     * @param $name
     * @param string $value
     * @return Input
     */
    public function addPasswordField($title, $name, $value = '')
    {
        $this->items[] = $input = new Input();
        $input->input_type = 'password';
        $input->name = $name;
        $input->title = $title;
        $input->value = $value;
        return $input;
    }

    /**
     * получение значения контрола после применения данных из запроса с клиента
     * @param string $input_name
     * @return string
     */
    public function getValue($input_name)
    {
        foreach ($this->items AS $item) {
            if ($item instanceof Controls\Select) {
                if ($item->name == $input_name) {
                    if ($option = $item->getSelectedOption()) {
                        return $option->value;
                    }
                }
                continue;
            }

            if ($item instanceof Input || $item instanceof Textarea) {
                if ($item->name == $input_name) {
                    return $item->value;
                }
                continue;
            }
        }
    }
}