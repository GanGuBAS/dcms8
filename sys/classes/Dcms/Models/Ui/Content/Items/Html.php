<?php
namespace Dcms\Models\Ui\Content\Items;

use Dcms\Models\Ui\Content\Item;

/**
 * Блок с содержимым страницы в виде HTML кода
 * Class Html
 */
class Html extends Item
{
    public $type = 'html';
    public $html;

    public function __construct($html = '')
    {
        parent::__construct();
        $this->html = $html;
    }
}