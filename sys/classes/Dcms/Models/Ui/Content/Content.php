<?php

namespace Dcms\Models\Ui\Content;
use Dcms\Core\UiModelRender;

/**
 * Содержимое страницы
 * Class page_content
 * @property Item[] items
 */
class Content extends UiModelRender
{
    public $items = array();

    /**
     * @return Items\Posts\Post
     */
    public function addPost()
    {
        return $this->items[] = new Items\Posts\Post();
    }

    /**
     * @return Items\Forms\Form
     */
    public function addForm()
    {
        return $this->items[] = new Items\Forms\Form();
    }
}