<?php

namespace Dcms\Models\Ui\Tabs;

use Dcms\Core\UiModelRender;
use Dcms\Core\Url;

/**
 * Вкладка модуля (страница).
 * Class Item
 * @property string url
 * @property string name
 * @property bool is_active
 */
class Item extends UiModelRender
{
    public
        $url,
        $name,
        $is_active;

    public function setName($name)
    {
        $this->name = $name;
    }

    public function setUrl($url)
    {
        $this->url = (string) new Url($url);
    }
}