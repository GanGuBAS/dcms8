<?php

namespace Dcms\Models\Ui\User;


/**
 * Меню пользователя (одноуровневое)
 * Class page_user_menu
 * @property Item[] $items
 */
class Menu extends \Dcms\Models\Ui\Menu\Menu
{
    public $items = array();

    /**
     * @param string $name
     * @param \Dcms\Core\Url|string $url
     * @return \Dcms\Models\Ui\Menu\Item|Item
     */
    public function addItem($name, $url)
    {
        $this->items[] = $menu = new Item();
        $menu->name = $name;
        $menu->setUrl($url);
        return $menu;
    }
}