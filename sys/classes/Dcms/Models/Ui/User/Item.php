<?php

namespace Dcms\Models\Ui\User;

use Dcms\Core\UiModelRender;
use Dcms\Core\Url;

/**
 * Class Item
 * @package Dcms\Models\Ui\User
 */
class Item extends UiModelRender
{
    public
        $url,
        $name;

    public function setUrl($url)
    {
        $this->url = (string) new Url($url);
    }
}