<?php

namespace Modules\Errors;

use Dcms\Core\Response\Response;
use Dcms\Core\Request\Request;

class Controller extends \Dcms\Core\Controller
{

    /**
     * Страницы ошибок
     * Прописаны в корневом .htaccess
     * @param Response $resp
     */
    public function GET_error(Response $resp)
    {
        $msg = $resp->page->messages;
        $resp->page->setTitle(__("Ошибка"));
        switch (Request::getParam('n')) {
            case 400:
                $msg->err(__("Неправильный запрос"));
                break;
            case 401:
                $msg->err(__("Необходима авторизация"));
                break;
            case 403:
                $msg->err(__("Доступ к запрошенной странице запрещен"));
                break;
            case 404:
                $msg->err(__("Страница не найдена"));
                break;
            case 413:
                $msg->err(__("Размер запроса слишком велик"));
                break;
            default:
                $msg->err(__("Неизвестная ошибка"));
                break;
        }
    }
}