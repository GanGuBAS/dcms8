<?php

namespace Modules\Icons;

use Dcms\Misc\FileSystem;

/**
 * Class Sprite
 * @package Modules\Icons
 * @property Icon[] $_icons
 */
class Sprite
{
    protected $_icons = array();
    protected $_replace_class = array(
        '.' => '_dot_',
        ',' => '_comma_',
        '+' => '_plus_',
        ':' => '_colon_',
        '[' => '_bracketl_',
        ']' => '_bracketr_',
        '{' => '_bracel_',
        '}' => '_bracer_'
    );
    protected
        $_width = 0, // ширина спрайта
        $_height = 0, // высота спрайта
        $_max_width = 512, // максимальная высота спрайта
        $_top_index = 0, // позиция для вставки следующей иконки
        $_left_index = 0; // позиция для вставки следующей иконки

    function addImage($path, $class_name = "")
    {
        if (!$class_name) {
            $class_name = basename($path, '.png');
        }
        $class_name = str_replace(array_keys($this->_replace_class), array_values($this->_replace_class), $class_name);

        foreach ($this->_icons AS $icon) {
            if ($icon->class === $class_name) {
                if (preg_match('/(.*?)([0-9]+)$/', $class_name, $matches)) {
                    return $this->addImage($path, $matches[1].($matches[2] + 1));
                } else {
                    return $this->addImage($path, $class_name.'2');
                }
            }
        }

        $icon = new Icon($path);
        $icon->class = $class_name;
        $this->_icons[] = $icon;
        return $class_name;
    }

    function cmp($i1, $i2)
    {
        if ($i1->h > $i2->h) {
            return 1;
        }
        if ($i1->h < $i2->h) {
            return -1;
        }
        return 0;
    }

    function bindIndexes()
    {
        usort($this->_icons, array($this, 'cmp'));

        $this->_width = 0;
        $this->_left_index = 0;
        $this->_top_index = 0;
        $this->_height = 0;

        for ($i = 0; $i < count($this->_icons); $i++) {
            $icon = $this->_icons[$i];

            if ($this->_left_index + $icon->w > $this->_max_width) {
                $this->_left_index = 0;
                $this->_top_index = $this->_height;
            }

            $icon->x = $this->_left_index;
            $icon->y = $this->_top_index;

            $this->_width = max($this->_width, $this->_left_index + $icon->w);
            $this->_height = max($this->_height, $this->_top_index + $icon->h);

            $this->_left_index += $icon->w;
        }
    }

    function saveSpriteImage($path)
    {
        $img = imagecreatetruecolor($this->_width, $this->_height);
        $black = imagecolorallocate($img, 0, 0, 0);
        imagecolortransparent($img, $black);
        imagealphablending($img, false);
        imagesavealpha($img, true);
        foreach ($this->_icons AS $icon) {
            imagecopy($img, $icon->getImage(), $icon->x, $icon->y, 0, 0, $icon->w, $icon->h);
        }
        imagepng($img, $path);
    }

    function saveSpriteCss($path, $sprite_src)
    {
        $css = '.icons{'."\n";
        $css .= 'display: inline-block;'."\n";
        $css .= 'overflow: hidden;'."\n";
        $css .= 'background-image: url('.$sprite_src.');'."\n";
        $css .= '}'."\n";

        foreach ($this->_icons AS $icon) {
            $css .= '.icons.'.$icon->class."{\n";
            $css .= 'width: '.$icon->w.'px;'."\n";
            $css .= 'height: '.$icon->h.'px;'."\n";
            $css .= 'background-position: -'.$icon->x.'px -'.$icon->y.'px;'."\n";
            $css .= "}\n";
        }

        FileSystem::fileWrite($path, $css, true, 0644);
    }
}