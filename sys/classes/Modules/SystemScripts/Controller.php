<?php

namespace Modules\SystemScripts;

use Dcms\Models\Ui\Meta\Meta;

class Controller extends \Dcms\Core\Controller
{

    /**
     *
     * @param Meta $page_meta
     */
    function THEME_DEP_dcms_client_model(Meta $page_meta)
    {
        $page_meta->scripts[] = ($this->getCurrentPathRel().'/files/dcms_client_model.js');
    }
}