<?php

namespace Modules\Breadcrumbs;

use Dcms\Core\Response\Response;
use Dcms\Core\Request\Request;
use Dcms\Models\Ui\Breadcrumbs\Item;

class Controller extends \Dcms\Core\Controller
{

    /**
     * @param $response Response
     */
    function POSTPROCESS_add_breadcrumbs(Response $response)
    {
        if (Request::getPath() != '/') {
            $home_breadcrumb = new Item(__('Главная'), '/');
            $home_breadcrumb->is_home = true;
            array_splice($response->page->breadcrumbs->items, 0, 0, array($home_breadcrumb));
        }
    }
}