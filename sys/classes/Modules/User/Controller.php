<?php

namespace Modules\User;

use Dcms\Core\Users;
use Dcms\Core\Response\Response;
use Dcms\Models\Ui\Content\Items\Forms\Form;

class Controller extends \Dcms\Core\Controller
{
    protected $_form;

    /**
     * @param $response Response
     */
    public function GET_info(Response $response)
    {
        $page = $response->page;
        $page->meta->title = __('Анкета пользователя');
    }

    /**
     * @param $response Response
     */
    public function GET_profile(Response $response)
    {
        $page = $response->page;
        $page->meta->title = __('Редактировать анкету');
    }

    /**
     * @param $response Response
     */
    public function POST_profile(Response $response)
    {

    }

    /**
     * @param $response Response
     */
    public function GET_settings(Response $response)
    {
        $page = $response->page;
        $page->meta->title = __('Изменение настроек');
    }

    /**
     * @param $response Response
     */
    public function POST_settings(Response $response)
    {

    }

    /**
     * @param $response \Dcms\Core\Response\Response
     * @param bool $is_auth
     */
    protected function _add_guest_tabs($response, $is_auth)
    {
        $page = $response->page;
        $page->tabs->addItem(__('Авторизация'), '/user/auth', $is_auth);
        $page->tabs->addItem(__('Регистрация'), '/user/reg', !$is_auth);
    }

    protected function _get_auth_form()
    {
        if (!$this->_form) {
            $this->_form = new Form();
            $this->_form->action = '/user/auth';
            $this->_form->addTextField(__('Логин'), 'login', '');
            $this->_form->addPasswordField(__('Пароль'), 'password', '');
            $this->_form->addButton(__('Авторизация'));
        }
        return $this->_form;
    }

    /**
     * @param $response Response
     */
    public function GET_auth(Response $response)
    {
        $page = $response->page;
        $page->setTitle(__('Авторизация'));
        $this->_add_guest_tabs($response, true);


        if (Users::isAuth()) {
            $post = $page->content->addPost();
            $post->setUrl('/');
            $post->setTitle(__('На главную'));
        } else {
            $page->content->items[] = $this->_get_auth_form();
        }
    }

    /**
     * @param $response Response
     */
    public function POST_auth(Response $response)
    {
        //$response->page->msg(__('Попытка авторизации'));
        $form = $this->_get_auth_form();

        $login = $form->getValue('login');
        $password = $form->getValue('password');

        try {
            Users::auth($login, $password);
            $response->page->msg(__('Вы успешно авторизованы'));
        } catch (\Exception $e) {
            $response->page->err($e->getMessage());
        }
    }

    protected function _get_reg_form()
    {
        if (!$this->_form) {
            $this->_form = new Form();
            $this->_form->action = '/user/reg';
            $this->_form->addTextField(__('Логин'), 'login', '');
            $this->_form->addPasswordField(__('Пароль'), 'password', '');
            $this->_form->addButton(__('Регистрация'));
        }
        return $this->_form;
    }

    /**
     * @param $response Response
     */
    public function GET_reg(Response $response)
    {
        $page = $response->page;
        $page->meta->title = __('Регистрация');
        $this->_add_guest_tabs($response, false);

        if (Users::isAuth()) {
            $post = $page->content->addPost();
            $post->setUrl('/');
            $post->setTitle(__('На главную'));
        } else {
            $page->content->items[] = $this->_get_reg_form();
        }
    }

    /**
     * @param $response Response
     */
    public function POST_reg(Response $response)
    {
        $form = $this->_get_reg_form();

        $login = $form->getValue('login');
        $password = $form->getValue('password');

        try {
            Users::reg($login, $password);
            $response->page->msg(__('Вы успешно зарегистрированы'));
        } catch (\Exception $e) {
            $response->page->err($e->getMessage());
        }
    }

    /**
     * @param $response Response
     */
    public function GET_logout(Response $response)
    {
        $response->page->setTitle(__('Выход с сайта'));
    }

    /**
     * @param $response Response
     */
    public function POST_logout(Response $response)
    {
        Users::logout();
    }

    /**
     * @param Response $response
     */
    public function POSTPROCESS_user(Response $response)
    {
        $response->page->user = Users::getCurrent();
        $user_menu = $response->page->user_menu;

        //if (!$user->group) {
        $user_menu->addItem(__('Авторизация'), '/user/auth');
        $user_menu->addItem(__('Регистрация'), '/user/reg');
//        } else {
//            $user_menu->addItem(__('Анкета'), '/user/id' . $user->id);
//            $user_menu->addItem(__('Профиль'), '/user/profile');
//            $user_menu->addItem(__('Настройки'), '/user/settings');
//            $user_menu->addItem(__('Выход'), '/user/logout');
//        }
    }
}