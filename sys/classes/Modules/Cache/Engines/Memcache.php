<?php

namespace Modules\Cache\Engines;

use Modules\Cache\ICacheEngine;

class Memcache implements ICacheEngine
{
    private $_instance;

    public function __construct()
    {
        $this->_instance = new \Memcache;
        if (!$this->_instance->connect('127.0.0.1', 11211)) {
            throw new \Exception(__("Не удалось подключиться к серверу Memcache"));
        }
    }

    public function clear($key)
    {
        $this->_instance->delete($key);
    }

    public function get($key)
    {
        return $this->_instance->get($key);
    }

    public function set($key, $content, $ttl)
    {
        $result = $this->_instance->replace($key, $content, false, $ttl);
        if ($result == false) {
            $result = $this->_instance->set($key, $content, false, $ttl);
        }
        return $result;
    }

    public static function canUsing()
    {
        return class_exists('\Memcache', false);
    }
}