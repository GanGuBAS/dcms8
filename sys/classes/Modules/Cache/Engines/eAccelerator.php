<?php

namespace Modules\Cache\Engines;

use Modules\Cache\ICacheEngine;

class eAccelerator implements ICacheEngine
{

    public function clear($key)
    {
        eaccelerator_rm($key);
    }

    public function get($key)
    {
        $data = eaccelerator_get($key);
        if (!$data) {
            return false;
        }
        return unserialize($data);
    }

    public function set($key, $content, $ttl)
    {
        return eaccelerator_put($key, serialize($content), $ttl);
    }

    public static function canUsing()
    {
        return extension_loaded('eaccelerator');
    }
}