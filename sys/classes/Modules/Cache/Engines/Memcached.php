<?php

namespace Modules\Cache\Engines;

use Modules\Cache\ICacheEngine;

class Memcached implements ICacheEngine
{
    private $_instance;

    public function __construct()
    {
        $this->_instance = new \Memcached();
    }

    public function clear($key)
    {
        $this->_instance->delete($key);
    }

    public function get($key)
    {
        return $this->_instance->get($key);
    }

    public function set($key, $content, $ttl)
    {
        return $this->set($key, $content, $ttl);
    }

    public static function canUsing()
    {
        return class_exists('\Memcached', false);
    }
}