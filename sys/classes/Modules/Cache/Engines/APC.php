<?php

namespace Modules\Cache\Engines;

use Modules\Cache\ICacheEngine;

class APC implements ICacheEngine
{

    public function clear($key)
    {
        apc_delete($key);
    }

    public function get($key)
    {
        return apc_fetch($key);
    }

    public function set($key, $content, $ttl)
    {
        return apc_store($key, $content, $ttl);
    }

    public static function canUsing()
    {
        return extension_loaded('apc');
    }
}