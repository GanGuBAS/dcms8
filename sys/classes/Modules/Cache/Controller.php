<?php

namespace Modules\Cache;

use Dcms\Models\Data\Module;

/**
 * @property ICacheEngine $_instance
 */
class Controller extends \Dcms\Core\Controller
{
    protected $_instance;

    function __construct(Module $module)
    {
        parent::__construct($module);

        $settings = $this->getSettings();
        $engine = $settings->get('engine', null);

        if (null === $engine) {
            $engine = $this->_autoselectEngine();
            $settings->set('engine', $engine);
        }

        if ($engine && class_exists($engine) && $engine instanceof ICacheEngine && $engine::canUsing()) {
            $this->_instance = new $engine();
        }
    }

    public function HOOK_setCache(&$data)
    {
        if (!$this->_instance) {
            throw new \Exception(__("Поддерживаемых движков кэширования не обнаружено"));
        }


        $content = $data['content'];
        $key = $data['key'];
        $ttl = $data['ttl'];

        $this->_instance->set($key, $content, $ttl);

        return false;
    }

    public function HOOK_getCache(&$data)
    {
        if (!$this->_instance) {
            throw new \Exception(__("Поддерживаемых движков кэширования не обнаружено"));
        }

        $key = $data['key'];

        $data['content'] = $this->_instance->get($key);

        return false;
    }

    public function HOOK_clearCache(&$data)
    {
        if (!$this->_instance) {
            throw new \Exception(__("Поддерживаемых движков кэширования не обнаружено"));
        }

        $key = $data['key'];

        $this->_instance->clear($key);

        return false;
    }

    private function _autoselectEngine()
    {
        $supportedEngines = $this->_getSupportingEngines();
        if (!count($supportedEngines)) {
            return false;
        }
        return $supportedEngines[0];
    }

    private function _getSupportingEngines()
    {
        $supportedEngines = array();
        $engines = $this->_getEngines();
        foreach ($engines as $className) {
            if ($className::canUsing()) {
                $supportedEngines[] = $className;
            }
        }
        return $supportedEngines;
    }

    /**
     * @return ICacheEngine[]
     */
    private function _getEngines()
    {
        $engines = array();
        $files = (array)glob($this->getCurrentPathAbs() . '/Engines/*.php');
        foreach ($files as $file_path) {
            $className = '\\Modules\\Cache\\Engines\\' . basename($file_path . '.php');
            if ($file_path && class_exists($className) && $className instanceof ICacheEngine) {
                $engines[] = $className;
            }
        }
        return $engines;
    }
}