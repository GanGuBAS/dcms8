<?php

namespace Modules\Cache;

interface ICacheEngine
{

    public function get($key);

    public function set($key, $content, $ttl);

    public function clear($key);

    static public function canUsing();
}