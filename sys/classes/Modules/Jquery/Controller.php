<?php

namespace Modules\Jquery;

use Dcms\Models\Ui\Meta\Meta;

class Controller extends \Dcms\Core\Controller
{

    /**
     * @param $page_meta Meta
     */
    function THEME_DEP_jquery(Meta $page_meta)
    {
        $page_meta->scripts[] = $this->getCurrentPathRel().'/files/jquery-2.1.1.min.js';
    }
}