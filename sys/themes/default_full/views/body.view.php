<?php
/**
 * @var $model \Dcms\Models\Ui\Page
 */
?>
<div id="container">
    <?php include __DIR__ . '/inc/content_item.ng-tpl.php'; ?>
    <?php include __DIR__ . '/inc/menu-item.ng-tpl.php'; ?>
    <div id="top_part">
        <header>
            <div class="cnt">
                <h1 id="logo" ng-bind="model.meta.title"><?= $model->meta->title ?></h1>

                <div id="tabs">
                    <span ng-repeat="i in model.tabs.items" data-href="{{i.url}}" ng-class="{active:i.is_active}"
                          ng-bind="i.name"></span>
                </div>
                <div id="user_menu" data-ng-if="model.user">
                    <span ng-if="model.user.login" ng-bind="model.user.login"><?= $model->user->login ?></span>
                    <span ng-if="!model.user.login"><?= __("Гость") ?></span>

                    <div id="user_menu_drop">
                        <span ng-repeat="i in model.user_menu.items" data-href="{{i.url}}" ng-bind="i.name"></span>
                    </div>
                </div>
            </div>
        </header>
        <div class="ctn">
            <div id="title">
                <div class="cnt">
                    <div id="breadcrumbs" ng-show="model.breadcrumbs.items.length">
                        <span ng-repeat="i in model.breadcrumbs.items" data-href="{{i.url}}" ng-bind="i.name"></span>
                    </div>
                </div>
            </div>

            <aside>
                <span class="menu" ng-repeat="data in model.menu.items" ng-include="'menu_item.html'"></span>
            </aside>

            <section>
                <div class="message" ng-repeat="m in model.messages.items" ng-class="{error:m.is_error}"
                     ng-bind="m.text"></div>
                <div class="content" ng-repeat="content in [model.content.items]"
                     ng-include="'content_item.html'"></div>
            </section>
        </div>
    </div>
</div>
<footer>
    <div class="cnt">
        Время генерации страницы: {{ model.meta.time_output - model.meta.time_start}}
    </div>
</footer>