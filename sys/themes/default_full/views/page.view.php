<?php
/**
 * @var $model \Dcms\Models\Ui\Page
 */
?><!DOCTYPE html>
<html xml:lang="ru" data-ng-app="Dcms" data-ng-controller="DcmsCtrl">
    <head>
        <?= $model->render('head.view') ?>
    </head>
    <body>
        <?= $model->render('body.view') ?>
    </body>
</html>