<div class="form" ng-if="data.type == 'form'">
    <form action="{{data.action}}" method="{{data.method}}" data-ng-controller="FormCtrl">
        <div class="F_control" data-ng-repeat="fi in data.items">
            <span class="FC_title" ng-if="fi.title" ng-bind="fi.title"></span>
            <select ng-if="fi.type == 'select'" name="{{fi.name}}">
                <option ng-repeat="option in fi.options" ng-selected="option.selected"
                        ng-value="option.value" ng-bind="option.text"></option>
            </select>

            <input ng-if="fi.type == 'input'" name="{{fi.name}}" type="{{fi.input_type}}"
                   ng-model="fi.value" ng-value="fi.value" ng-click="form.onInputClick($event)"/>
            <textarea ng-if="fi.type == 'textarea'" name="{{fi.name}}" ng-model="fi.value"></textarea>

            <div class="content" ng-if="data.content_items.length > 0" ng-init="content = data.content_items"
                 ng-include="'content_item.html'"></div>
        </div>
    </form>
</div>